def phi(s):
#     return np.arctan(sat.position[1][i]/sat.position[0][i])
    return float(s)/6300

def f(s):
    vx = float(s)
    # vx = satellite.V[0][i]
    # vy = satellite.V[1][i]
#     return log((vx**2+vy**2)**(1/2))
#     return (vx**2+vy**2)**(1/2)
    return vx

def get_derivatives(s):
    phi_a = abs(phi(s[5]))
    phi_d = abs(phi(s[14]))
    phi_e = abs(phi(s[23]))
    
    f_a = f(s[1])
    f_d = f(s[10])
    f_e = f(s[19])
    
    b_a = abs(float(s[7]))
    b_d = abs(float(s[16]))
    b_e = abs(float(s[25]))
    
    denominator = (phi_a-phi_e)*(b_a-b_d)-(phi_a-phi_d)*(b_a-b_e)
    nominatorb = (phi_a-phi_e)*(f_a-f_d)-(f_a-f_e)*(phi_a-phi_d)
    nominatorphi = (f_a-f_e)*(b_a-b_d)-(f_a-f_d)*(b_a-b_e)
    x = nominatorphi/denominator
    y = nominatorb/denominator

    return x, y, denominator

import csv
from datetime import datetime

def perform_all():

    filenamein = 'SatelliteDataDate.csv'
    filenameout = 'SatelliteDataDateR.csv'
    outfile = open(filenameout, "w")

    with open(filenamein) as csvfile1:
        
        readCSV = csv.reader(csvfile1, delimiter=';')
        ind = 0
        for s in readCSV:
            Vx_ave = (float(s[1]) + float(s[10]) + float(s[19])) / 3
            Bx_ave = (float(s[7]) + float(s[16]) + float(s[25])) / 3
            Y_ave = (float(s[5]) + float(s[14]) + float(s[23])) / 3

            line = s

            line.append(Vx_ave) # line[28]
            line.append(Bx_ave) # line[29]
            line.append(Y_ave) # line[30]

            df_dphi, df_db, denominator = get_derivatives(s)

            line.append(df_dphi) # line[31]
            line.append(df_db) # line[32]
            line.append(denominator) # line[33]

            m = (';').join([str(item) for item in line])
            m = m + '\n'
            outfile.write(m)

    outfile.close()

perform_all()