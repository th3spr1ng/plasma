# %%
import matplotlib
import datetime
import csv
import pandas as pd
import numpy as np
import time as timeall
import matplotlib.pyplot as plt
from math import log, log10
from spacepy import pycdf
from scipy.interpolate import CubicSpline
from time import gmtime, strftime, ctime

# %%
def float_to_dates(x, freq = 1):
    datess = []
    for i in range(0,len(x),freq):
        time = x[i]
        datess.append(datetime.datetime.utcfromtimestamp(time))
    return datess

def dates_to_float(d):
    datess = []
    for i in range(0,len(d)):
        epoch = datetime.datetime.utcfromtimestamp(0)
        total_seconds =  (d[i] - epoch).total_seconds()
        datess.append(total_seconds)
    return datess

def float_to_date(x):
    datess = (datetime.datetime.utcfromtimestamp(x))
    return datess

def date_to_float(d):
    epoch = datetime.datetime.utcfromtimestamp(0)
    total_seconds =  (d - epoch).total_seconds()
    return total_seconds

def interpolate(x, times, resulting_time):
    fx = CubicSpline(times, x, extrapolate=True)
    
    fxa = []

    for i in resulting_time:
        lx = fx(i).tolist()
        fxa.append(lx)
    
    return fxa

def isSorted(array):
    r = True
    for i in range(1, len(array)):
        if (array[i] < array[i-1]):
            r = False
            break
            
    return r

def isIncreasing(array):
    r = True
    for i in range(1, len(array)):
        if (array[i] <= array[i-1]):
            r = False
            print('\n\n\n\n\n\n\n#################\nI am actually here:\n')
            print(array[i-1],array[i])
            break
            
    return r

# %%
def fill_x(name, filename, terms):
    cdf = pycdf.CDF(filename)
    
    x = []
    y = []
    z = []
    times = []

    for i in range(0,len(cdf['th' + name + '_pos_gsm'])):    # store positions x,y,z
        position = cdf['th' + name + '_pos_gsm'][i]
        
        if np.all(np.isfinite(position)) and np.all(np.nan != position):
            x.append(cdf['th' + name + '_pos_gsm'][i][0])
            y.append(cdf['th' + name + '_pos_gsm'][i][1])
            z.append(cdf['th' + name + '_pos_gsm'][i][2])
            times.append(cdf['th' + name + '_state_time'][i])

    # In this cells I do an interpolation for
    # Position of spacecraft
    
    try:
        t2 = np.linspace(times[0], times[-1], terms)
    except IndexError:
        print("no occurances at all")
        return [x,y,z], []
    else:
        x = interpolate(x, times, t2)
        y = interpolate(y, times, t2)
        z = interpolate(z, times, t2)

        pos_info = [x,y,z]

        cdf.close()

        return pos_info, t2   # As a result I can take information
                          # about position from file
    
def fill_x_second(name, filename, resulting_time):
    cdf = pycdf.CDF(filename)
    
    x = []
    y = []
    z = []
    times = []

    for i in range(0,len(cdf['th' + name + '_pos_gsm'])):    # store positions x,y,z
        position = cdf['th' + name + '_pos_gsm'][i]
        time = cdf['th' + name + '_state_time'][i]
        if ((time >= resulting_time[0]) and (time <= resulting_time[-1])):
            if np.all(np.isfinite(position)) and np.all(np.nan != position):
                x.append(cdf['th' + name + '_pos_gsm'][i][0])
                y.append(cdf['th' + name + '_pos_gsm'][i][1])
                z.append(cdf['th' + name + '_pos_gsm'][i][2])
                times.append(cdf['th' + name + '_state_time'][i])

    pos_info = [x,y,z]    
    cdf.close()
    
    return pos_info, times
    
def fill_v(name, filename, resulting_time):
    cdf = pycdf.CDF(filename)
    
    vx = []
    vy = []
    vz = []
    times = []
    prev_t = -1000000000000
    
    for i in range(0,len(cdf['th' + name + '_peim_velocity_gsm'])):
        velocity = cdf['th' + name + '_peim_velocity_gsm'][i]
        time = cdf['th' + name + '_peim_time'][i]
        if (time >= resulting_time[0] and time <= resulting_time[-1]):
            if np.all(np.isfinite(velocity)) and np.all(np.nan != velocity):
                m = cdf['th' + name + '_peim_time'][i]
                if (m > prev_t):
                    vx.append(velocity[0])
                    vy.append(velocity[1])
                    vz.append(velocity[2])
                    times.append(cdf['th' + name + '_peim_time'][i])
                    prev_t = m

    vel_info = [vx,vy,vz]
    cdf.close()
    return vel_info, times

def fill_magn(name, filename, resulting_time):
    cdf = pycdf.CDF(filename)
    
    times = []
    bx = []
    by = []
    bz = []
    prev_t = -1000000000000
 

    for i in range(0,len(cdf['th' + name + '_fgs_gsm'])):
        m = cdf['th' + name + '_fgs_time'][i]
        if (m > prev_t):
            prev_t = m
            field = cdf['th' + name + '_fgs_gsm'][i]
            time = cdf['th' + name + '_fgs_time'][i]
            if (time >= resulting_time[0] and time <= resulting_time[-1]):
                if (field[0] and field[1] and field[2]) is not np.nan:
                    bx.append(field[0])
                    by.append(field[1])
                    bz.append(field[2])
                    times.append(cdf['th' + name + '_fgs_time'][i])
    
    cdf.close()
    
    field = [bx, by, bz]
    return field, times

# %%
class Satellite:
    def __init__(self, date, satellite_name):   # date as datetime in string; satellite name as string: T_a
                                          # date looks like 20190422 for 24'th april of 2019
        
        file_vel = '/home/fedor/Documents/HSE/Projects/Data/Data_Retrieve/T_' + satellite_name + '/th' + satellite_name + '_l2_mom_' + date + '_v01.cdf'
        file_magn = '/home/fedor/Documents/HSE/Projects/Data/Data_Retrieve/T_' + satellite_name + '/th' + satellite_name + '_l2_fgm_' + date + '_v01.cdf'
        file_pos = '/home/fedor/Documents/HSE/Projects/Data/Data_Retrieve/T_' + satellite_name + '/th' + satellite_name + '_l1_state_' + date + '_v00.cdf'
        
        cdf2 = pycdf.CDF(file_magn)
        terms = len(cdf2['th' + satellite_name + '_fgs_gsm'])
        resulting_time = cdf2['th' + satellite_name + '_fgs_time']
        sat_name = satellite_name
        
        self.position, self.time = fill_x(sat_name, file_pos, terms)
        print('self time is', len(self.time))
        print('self position is',len(self.position[0]))
        
        if (len(self.time) == 0):
            beg, end = 0, 0
        else:
            beg, end = get_interval_for_position(self.position)
        
        self.time_interval = self.time[beg:end]
        print('len of interval is', len(self.time_interval), 'points')
    
    def add_info(self, date, satellite_name, time_res):
        
        file_vel = '/home/fedor/Documents/HSE/Projects/Data/Data_Retrieve/T_' + satellite_name + '/th' + satellite_name + '_l2_mom_' + date + '_v01.cdf'
        file_magn = '/home/fedor/Documents/HSE/Projects/Data/Data_Retrieve/T_' + satellite_name + '/th' + satellite_name + '_l2_fgm_' + date + '_v01.cdf'
        file_pos = '/home/fedor/Documents/HSE/Projects/Data/Data_Retrieve/T_' + satellite_name + '/th' + satellite_name + '_l1_state_' + date + '_v00.cdf'
        
        cdf2 = pycdf.CDF(file_magn)
        terms = len(cdf2['th' + satellite_name + '_fgs_gsm'])
        resulting_time = time_res
        terms = len(resulting_time)
        sat_name = satellite_name
        
        self.position, self.time = fill_x_second(sat_name, file_pos, resulting_time)
        
        self.velocity, self.vtime = fill_v(sat_name, file_vel, resulting_time)
        self.magnitude, self.mtime = fill_magn(sat_name, file_magn, resulting_time)
        
class Day:
    def __init__(self, date):
        self.date_of_day = date
        self.sat_a = Satellite(date, 'a')
        self.sat_d = Satellite(date, 'd')
        self.sat_e = Satellite(date, 'e')
        
        self.Flag = False
        
        try:
            beg_interval = max(self.sat_a.time_interval[0], self.sat_d.time_interval[0], self.sat_e.time_interval[0])
            end_interval = min(self.sat_a.time_interval[-1], self.sat_d.time_interval[-1], self.sat_e.time_interval[-1])
        except IndexError:
            self.Flag = True
            
        if (self.Flag == False):
            l = len(self.sat_a.time_interval)

            self.time = np.linspace(beg_interval, end_interval, num=l)
            
            self.sat_a.add_info(self.date_of_day, 'a', self.time)
            self.sat_d.add_info(self.date_of_day, 'd', self.time)
            self.sat_e.add_info(self.date_of_day, 'e', self.time)

            self.beg = 0
            self.end = 0

# %%
def get_interval_for_position(position):
    begm = 0         # For minimal beginning
    endm = 0         # For minimal ending
    
    begcurr = 0      # For current beginning
    endcurr = 0      # For current ending
    
    minlen = 0
    currlen = 0
    
    arrx = position[0]
    arry = position[1]
    
    for i in range(1,len(arrx)):
        in_interval = False

        if ((arrx[i] < -44660) and (abs(arry[i]) < abs(arrx[i]))):
            if (currlen == 0):
                begcurr = i - 1
                endcurr = i
                
            currlen += 1
            endcurr += 1
            in_interval = True
        
        if (in_interval == False):
            if minlen < currlen:
                minlen = currlen
                begm = begcurr
                endm = endcurr
                currlen = 0
                
    if minlen < currlen:
            minlen = currlen
            begm = begcurr
            endm = endcurr
            currlen = 0
    
    print('goes from to', begm, endm)
    return begm, endm

# %%
def interpolate_satellites(day):
    time = day.time
    
    day.sat_a.position[0] = interpolate(day.sat_a.position[0], day.sat_a.time, time)
    day.sat_a.position[1] = interpolate(day.sat_a.position[1], day.sat_a.time, time)
    day.sat_a.position[2] = interpolate(day.sat_a.position[2], day.sat_a.time, time)
    day.sat_d.position[0] = interpolate(day.sat_d.position[0], day.sat_d.time, time)
    day.sat_d.position[1] = interpolate(day.sat_d.position[1], day.sat_d.time, time)
    day.sat_d.position[2] = interpolate(day.sat_d.position[2], day.sat_d.time, time)
    day.sat_e.position[0] = interpolate(day.sat_e.position[0], day.sat_e.time, time)
    day.sat_e.position[1] = interpolate(day.sat_e.position[1], day.sat_e.time, time)
    day.sat_e.position[2] = interpolate(day.sat_e.position[2], day.sat_e.time, time)
    
    day.sat_a.velocity[0] = interpolate(day.sat_a.velocity[0], day.sat_a.vtime, time)
    day.sat_a.velocity[1] = interpolate(day.sat_a.velocity[1], day.sat_a.vtime, time)
    day.sat_a.velocity[2] = interpolate(day.sat_a.velocity[2], day.sat_a.vtime, time)
    day.sat_d.velocity[0] = interpolate(day.sat_d.velocity[0], day.sat_d.vtime, time)
    day.sat_d.velocity[1] = interpolate(day.sat_d.velocity[1], day.sat_d.vtime, time)
    day.sat_d.velocity[2] = interpolate(day.sat_d.velocity[2], day.sat_d.vtime, time)
    day.sat_e.velocity[0] = interpolate(day.sat_e.velocity[0], day.sat_e.vtime, time)
    day.sat_e.velocity[1] = interpolate(day.sat_e.velocity[1], day.sat_e.vtime, time)
    day.sat_e.velocity[2] = interpolate(day.sat_e.velocity[2], day.sat_e.vtime, time)
    
    day.sat_a.magnitude[0] = interpolate(day.sat_a.magnitude[0], day.sat_a.mtime, time)
    day.sat_a.magnitude[1] = interpolate(day.sat_a.magnitude[1], day.sat_a.mtime, time)
    day.sat_a.magnitude[2] = interpolate(day.sat_a.magnitude[2], day.sat_a.mtime, time)
    day.sat_d.magnitude[0] = interpolate(day.sat_d.magnitude[0], day.sat_d.mtime, time)
    day.sat_d.magnitude[1] = interpolate(day.sat_d.magnitude[1], day.sat_d.mtime, time)
    day.sat_d.magnitude[2] = interpolate(day.sat_d.magnitude[2], day.sat_d.mtime, time)
    day.sat_e.magnitude[0] = interpolate(day.sat_e.magnitude[0], day.sat_e.mtime, time)
    day.sat_e.magnitude[1] = interpolate(day.sat_e.magnitude[1], day.sat_e.mtime, time)
    day.sat_e.magnitude[2] = interpolate(day.sat_e.magnitude[2], day.sat_e.mtime, time)
    
    day.sat_d.time = time
    day.sat_e.time = time

# %%
def write_to_file(day):
    filename = 'SatelliteDataDate2011.csv'
    f = open(filename, 'a')

    for i in range(len(day.time)):
        s_day = str(float_to_date(day.time[i]))

        a = day.sat_a

        s_a_v = str(a.velocity[0][i]) + ';' + str(a.velocity[1][i]) + ';' + str(a.velocity[2][i])
        s_a_p = str(a.position[0][i]) + ';' + str(a.position[1][i]) + ';' + str(a.position[2][i])
        s_a_b = str(a.magnitude[0][i]) + ';' + str(a.magnitude[1][i]) + ';' + str(a.magnitude[2][i])
        s_a = s_a_v + ';' + s_a_p + ';' + s_a_b

        d = day.sat_d

        s_d_v = str(d.velocity[0][i]) + ';' + str(d.velocity[1][i]) + ';' + str(d.velocity[2][i])
        s_d_p = str(d.position[0][i]) + ';' + str(d.position[1][i]) + ';' + str(d.position[2][i])
        s_d_b = str(d.magnitude[0][i]) + ';' + str(d.magnitude[1][i]) + ';' + str(d.magnitude[2][i])
        s_d = s_d_v + ';' + s_d_p + ';' + s_d_b

        e = day.sat_e

        s_e_v = str(e.velocity[0][i]) + ';' + str(e.velocity[1][i]) + ';' + str(e.velocity[2][i])
        s_e_p = str(e.position[0][i]) + ';' + str(e.position[1][i]) + ';' + str(e.position[2][i])
        s_e_b = str(e.magnitude[0][i]) + ';' + str(e.magnitude[1][i]) + ';' + str(e.magnitude[2][i])
        s_e = s_e_v + ';' + s_e_p + ';' + s_e_b

        s_day = s_day + ';' + s_a + ';' + s_d + ';' + s_e + '\n'

        f.write(s_day)
            
    f.close()

# %%
def perform_everything(d):
    print('day', d)

    try:
        day = Day(d)
    except pycdf.CDFError:
        print("Can't read files")
    else:
        if (day.Flag == False):
            try:
                interpolate_satellites(day)
            except ValueError:
                print('Something went wrong with sorting of time', d)

            else:
                print(len(day.sat_a.velocity[0]))
                print(len(day.sat_a.position[0]))
                print(len(day.sat_e.velocity[0]))
                print(len(day.sat_e.position[0]))

                write_to_file(day)

# %%
filename = 'SatelliteDataDate2011.csv'

f = open(filename, 'w')
    
day_list = [
    '20110101',
    '20110102',
    '20110103',
    '20110104',
    '20110105',
    '20110106',
    '20110107',
    '20110108',
    '20110109',
    '20110110',
    '20110111',
    '20110112',
    '20110113',
    '20110114',
    '20110115',
    '20110116',
    '20110117',
    '20110118',
    '20110119',
    '20110120',
    '20110121',
    '20110122',
    '20110123',
    '20110124',
    '20110125',
    '20110126',
    '20110127',
    '20110128',
    '20110129',
    '20110130',
    '20110131',
    '20110201',
    '20110202',
    '20110203',
    '20110204',
    '20110205',
    '20110206',
    '20110207',
    '20110208',
    '20110209',
    '20110210',
    '20110211',
    '20110212',
    '20110213',
    '20110214',
    '20110215',
    '20110216',
    '20110217',
    '20110218',
    '20110219',
    '20110220',
    '20110221',
    '20110222',
    '20110223',
    '20110224',
    '20110225',
    '20110226',
    '20110227',
    '20110228',
    '20110301',
    '20110302',
    '20110303',
    '20110304',
    '20110305',
    '20110306',
    '20110307',
    '20110308',
    '20110309',
    '20110310',
    '20110311',
    '20110312',
    '20110313',
    '20110314',
    '20110315',
    '20110316',
    '20110317',
    '20110318',
    '20110319',
    '20110320',
    '20110321',
    '20110322',
    '20110323',
    '20110324',
    '20110325',
    '20110326',
    '20110327',
    '20110328',
    '20110329',
    '20110330',
    '20110331',
    '20110401',
    '20110402',
    '20110403',
    '20110404',
    '20110405',
    '20110406',
    '20110407',
    '20110408',
    '20110409',
    '20110410',
    '20110411',
    '20110412',
    '20110413',
    '20110414',
    '20110415',
    '20110416',
    '20110417',
    '20110418',
    '20110419',
    '20110420',
    '20110421',
    '20110422',
    '20110423',
    '20110424',
    '20110425',
    '20110426',
    '20110427',
    '20110428',
    '20110429',
    '20110430',
    '20110501',
    '20110502',
    '20110503',
    '20110504',
    '20110505',
    '20110506',
    '20110507',
    '20110508',
    '20110509',
    '20110510',
    '20110511',
    '20110512',
    '20110513',
    '20110514',
    '20110515',
    '20110516',
    '20110517',
    '20110518',
    '20110519',
    '20110520',
    '20110521',
    '20110522',
    '20110523',
    '20110524',
    '20110525',
    '20110526',
    '20110527',
    '20110528',
    '20110529',
    '20110530',
    '20110531',
    '20110601',
    '20110602',
    '20110603',
    '20110604',
    '20110605',
    '20110606',
    '20110607',
    '20110608',
    '20110609',
    '20110610',
    '20110611',
    '20110612',
    '20110613',
    '20110614',
    '20110615',
    '20110616',
    '20110617',
    '20110618',
    '20110619',
    '20110620',
    '20110621',
    '20110622',
    '20110623',
    '20110624',
    '20110625',
    '20110626',
    '20110627',
    '20110628',
    '20110629',
    '20110630',
    '20110701',
    '20110702',
    '20110703',
    '20110704',
    '20110705',
    '20110706',
    '20110707',
    '20110708',
    '20110709',
    '20110710',
    '20110711',
    '20110712',
    '20110713',
    '20110714',
    '20110715',
    '20110716',
    '20110717',
    '20110718',
    '20110719',
    '20110720',
    '20110721',
    '20110722',
    '20110723',
    '20110724',
    '20110725',
    '20110726',
    '20110727',
    '20110728',
    '20110729',
    '20110730',
    '20110731',
    '20110801',
    '20110802',
    '20110803',
    '20110804',
    '20110805',
    '20110806',
    '20110807',
    '20110808',
    '20110809',
    '20110810',
    '20110811',
    '20110812',
    '20110813',
    '20110814',
    '20110815',
    '20110816',
    '20110817',
    '20110818',
    '20110819',
    '20110820',
    '20110821',
    '20110822',
    '20110823',
    '20110824',
    '20110825',
    '20110826',
    '20110827',
    '20110828',
    '20110829',
    '20110830',
    '20110831',
    '20110901',
    '20110902',
    '20110903',
    '20110904',
    '20110905',
    '20110906',
    '20110907',
    '20110908',
    '20110909',
    '20110910',
    '20110911',
    '20110912',
    '20110913',
    '20110914',
    '20110915',
    '20110916',
    '20110917',
    '20110918',
    '20110919',
    '20110920',
    '20110921',
    '20110922',
    '20110923',
    '20110924',
    '20110925',
    '20110926',
    '20110927',
    '20110928',
    '20110929',
    '20110930',
    '20111001',
    '20111002',
    '20111003',
    '20111004',
    '20111005',
    '20111006',
    '20111007',
    '20111008',
    '20111009',
    '20111010',
    '20111011',
    '20111012',
    '20111013',
    '20111014',
    '20111015',
    '20111016',
    '20111017',
    '20111018',
    '20111019',
    '20111020',
    '20111021',
    '20111022',
    '20111023',
    '20111024',
    '20111025',
    '20111026',
    '20111027',
    '20111028',
    '20111029',
    '20111030',
    '20111031',
    '20111101',
    '20111102',
    '20111103',
    '20111104',
    '20111105',
    '20111106',
    '20111107',
    '20111108',
    '20111109',
    '20111110',
    '20111111',
    '20111112',
    '20111113',
    '20111114',
    '20111115',
    '20111116',
    '20111117',
    '20111118',
    '20111119',
    '20111120',
    '20111121',
    '20111122',
    '20111123',
    '20111124',
    '20111125',
    '20111126',
    '20111127',
    '20111128',
    '20111129',
    '20111130',
    '20111201',
    '20111202',
    '20111203',
    '20111204',
    '20111205',
    '20111206',
    '20111207',
    '20111208',
    '20111209',
    '20111210',
    '20111211',
    '20111212',
    '20111213',
    '20111214',
    '20111215',
    '20111216',
    '20111217',
    '20111218',
    '20111219',
    '20111220',
    '20111221',
    '20111222',
    '20111223',
    '20111224',
    '20111225',
    '20111226',
    '20111227',
    '20111228',
    '20111229',
    '20111230',
    '20111231'
]

for d in day_list:
    perform_everything(d)