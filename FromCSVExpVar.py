# %%
import matplotlib
import datetime
import pandas as pd
import numpy as np
import time as timeall
import matplotlib.pyplot as plt
from math import log, log10
from scipy.interpolate import CubicSpline
from time import gmtime, strftime, ctime

# %%
def float_to_dates(x, freq = 1):
    datess = []
    for i in range(0,len(x),freq):
        time = x[i]
        datess.append(datetime.datetime.utcfromtimestamp(time))
    return datess

def dates_to_float(d):
    datess = []
    for i in range(0,len(d)):
        epoch = datetime.datetime.utcfromtimestamp(0)
        total_seconds =  (d[i] - epoch).total_seconds()
        datess.append(total_seconds)
    return datess

def float_to_date(x):
    datess = (datetime.datetime.utcfromtimestamp(x))
    return datess

def date_to_float(d):
    epoch = datetime.datetime.utcfromtimestamp(0)
    total_seconds =  (d - epoch).total_seconds()
    return total_seconds

def date_to_float2(d):
    epoch = datetime.utcfromtimestamp(0)
    total_seconds =  (d - epoch).total_seconds()
    return total_seconds


def interpolate(x, times, terms, resulting_time):
    fx = CubicSpline(times, x, extrapolate=True)

    fxa = []

    for i in resulting_time:
        lx = fx(i).tolist()
        fxa.append(lx)
    
    return fxa

def isSorted(array):
    r = True
    for i in range(1, len(array)):
        if (array[i] < array[i-1]):
            r = False
            break
            
    return r

def isIncreasing(array):
    r = True
    for i in range(1, len(array)):
        if (array[i] <= array[i-1]):
            r = False
            print('\n\n\n\n\n\n\n#################\nI am actually here:\n')
            print(array[i-1],array[i])
            break
            
    return r

# %%
class Satellite:
    def __init__(self, date, satellite_name):   # date as datetime in string; satellite name as string: T_a
                                          # date looks like 20190422 for 24'th april of 2019
        self.pos = [[],[],[]]
        self.V = [[],[],[]]
        self.B = [[],[],[]]
        
class Day:
    def __init__(self, date):
        self.date = date
        self.time = []
        self.sat_a = Satellite(date, 'a')
        self.sat_d = Satellite(date, 'd')
        self.sat_e = Satellite(date, 'e')
        self.phi = []
        self.T = []
        self.f = []
        self.df_dB = []
        self.df_dphi = []
        self.df_dT = []
        self.indeces = []
        self.denominator = []
        self.Vx_ave = []
        self.Bx_ave = []
        self.phi_ave = []

# %%
def phi(sat, i):
#     return np.arctan(sat.position[1][i]/sat.position[0][i])
    return sat.pos[1][i]/6300

def f(satellite, i):
    vx = satellite.V[0][i]
    vy = satellite.V[1][i]
#     return log((vx**2+vy**2)**(1/2))
#     return (vx**2+vy**2)**(1/2)
    return vx

# %%
def get_range(B_i, phi_i, B_s, phi_s):
    return ((B_i-B_s)**2 + (phi_i-phi_s)**2)**(1/2)

def fillindeces_and_all(day):
    axis = 'x'
    typ = 2
    if (axis == 'x'):
        typ = 0
        
    for i in range(len(day.time)):
        b_a = abs(day.sat_a.B[0][i])
        b_d = abs(day.sat_d.B[0][i])
        b_e = abs(day.sat_e.B[0][i])
        
        reg_phi_a = phi(day.sat_a, i)
        reg_phi_d = phi(day.sat_d, i)
        reg_phi_e = phi(day.sat_e, i)
        
        phi_a = abs(reg_phi_a)
        phi_d = abs(reg_phi_d)
        phi_e = abs(reg_phi_e)
        
        f_a = f(day.sat_a, i)
        f_d = f(day.sat_d, i)
        f_e = f(day.sat_e, i)

        dphi, db = day.df_dphi[i], day.df_dB[i]
        b_ave = day.Bx_ave[i]
        phi_ave = day.phi_ave[i]/6300

        B_k = [b_a/b_ave, b_d/b_ave, b_e/b_ave]
        phi_k = [phi_a/phi_ave, phi_d/phi_ave, phi_e/phi_ave]

        ranges = []

        for k in range(0,3):
            ranges.append(get_range(B_k[k], phi_k[k], B_k[k-1], phi_k[k-1]))
        
        day.f.append(day.Vx_ave[i])
        day.phi.append(day.phi_ave[i])
        
        d = day.denominator[i]
    
        if (max(ranges)/min(ranges) > 5 or (abs(d) <= 0.001)):
            day.indeces.append(i)
    
def delete_indeces(day):
    indeces = day.indeces
    k = len(indeces)
    for i in range(k - 1, -1, -1):
        del day.sat_a.V[0][indeces[i]]
        del day.sat_a.V[1][indeces[i]]
        del day.sat_a.V[2][indeces[i]]
        
        del day.sat_a.B[0][indeces[i]]
        del day.sat_a.B[1][indeces[i]]
        del day.sat_a.B[2][indeces[i]]
        
        del day.sat_a.pos[0][indeces[i]]
        del day.sat_a.pos[1][indeces[i]]
        del day.sat_a.pos[2][indeces[i]]
        
        del day.sat_d.V[0][indeces[i]]
        del day.sat_d.V[1][indeces[i]]
        del day.sat_d.V[2][indeces[i]]
        
        del day.sat_d.B[0][indeces[i]]
        del day.sat_d.B[1][indeces[i]]
        del day.sat_d.B[2][indeces[i]]
        
        del day.sat_d.pos[0][indeces[i]]
        del day.sat_d.pos[1][indeces[i]]
        del day.sat_d.pos[2][indeces[i]]
        
        del day.sat_e.V[0][indeces[i]]
        del day.sat_e.V[1][indeces[i]]
        del day.sat_e.V[2][indeces[i]]
        
        del day.sat_e.B[0][indeces[i]]
        del day.sat_e.B[1][indeces[i]]
        del day.sat_e.B[2][indeces[i]]
        
        del day.sat_e.pos[0][indeces[i]]
        del day.sat_e.pos[1][indeces[i]]
        del day.sat_e.pos[2][indeces[i]]
        
        del day.time[indeces[i]]
        del day.df_dphi[indeces[i]]
        del day.df_dB[indeces[i]]
        del day.f[indeces[i]]
        del day.phi[indeces[i]]
        del day.Bx_ave[indeces[i]]
        del day.phi_ave[indeces[i]]
        del day.denominator[indeces[i]]
        del day.Vx_ave[indeces[i]]
        print('deleted smth')

# %%
# datetime.datetime(year, month, day, hours, mins, seconds, msecs)
def find_timestamp(time, timestampbeg, timestampend):
    tbeg = 0
    tend = 0
    
    for i in range(len(time)):
        if (time[i] >= timestampbeg and tbeg == 0):
            tbeg = i
            
        if (time[i] >= timestampend and tend == 0):
            tend = i
            
    return tbeg, tend

# %%
def build_v_b_ph_plots(day):
    phi_a, phi_d, phi_e = [], [], []
    b_x_a, b_x_d, b_x_e = [], [], []
    v_x_a, v_x_d, v_x_e = [], [], []
    timepl = []

    bega = 0
    enda = len(day.time) - 1

    for i in  range(len(day.time)):
        timepl.append(datetime.utcfromtimestamp(day.time[i]))

        v_x_a.append(day.sat_a.V[0][i])
        v_x_d.append(day.sat_d.V[0][i])
        v_x_e.append(day.sat_e.V[0][i])

        b_x_a.append(day.sat_a.B[0][i])
        b_x_d.append(day.sat_d.B[0][i])
        b_x_e.append(day.sat_e.B[0][i])

        phi_a.append(phi(day.sat_a, i))
        phi_d.append(phi(day.sat_d, i))
        phi_e.append(phi(day.sat_e, i))

    day.timepl = timepl

    # Plotting V
    fig, ax = plt.subplots(figsize=(27.2, 9))

    ax.plot_date(timepl, v_x_a, color='red', ls='-', lw=0.6,mew=0.6,ms=0.6)
    ax.plot_date(timepl, v_x_d, color='orange', ls='-', lw=0.6,mew=0.6,ms=0.6)
    ax.plot_date(timepl, v_x_e, color='blue', ls='-', lw=0.6,mew=0.6,ms=0.6)

    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%H:%M"))

    label_list = [r'$v_{a_x}$',r'$v_{d_x}$',r'$v_{e_x}$']

    plt.title(r'$v_x$', fontsize=30)
    plt.legend(label_list, prop={'size': 30})
    # plt.yscale('symlog')
    ax.tick_params(axis='both', which='major', labelsize=20)

    plt.show()

    # Plotting magnetic field |B_x|

    fig, ax = plt.subplots(figsize=(27.2, 9))

    ax.plot_date(timepl, b_x_a, color='red', ls='-', lw=0.6,mew=0.6,ms=0.6)
    ax.plot_date(timepl, b_x_d, color='orange', ls='-', lw=0.6,mew=0.6,ms=0.6)
    ax.plot_date(timepl, b_x_e, color='blue', ls='-', lw=0.6,mew=0.6,ms=0.6)

    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%H:%M"))

    label_list = [r'$|B_{a_x}|$',r'$|B_{d_x}|$',r'$|B_{e_x}|$']
    tit = r'$B_x$'

    plt.title(tit, fontsize=30)
    plt.legend(label_list, prop={'size': 30})
    ax.tick_params(axis='both', which='major', labelsize=20)

    plt.show()

    # Plotting phi

    fig, ax = plt.subplots(figsize=(27.2, 9))

    ax.plot_date(timepl, phi_a, color='red', ls='-', lw=0.6,mew=0.6,ms=0.6)
    ax.plot_date(timepl, phi_d, color='orange', ls='-', lw=0.6,mew=0.6,ms=0.6)
    ax.plot_date(timepl, phi_e, color='blue', ls='-', lw=0.6,mew=0.6,ms=0.6)

    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%H:%M"))

    label_list = [r'$y_a$',r'$y_d$',r'$y_e$']

    plt.title(r'$y$', fontsize=30)
    plt.legend(label_list, prop={'size': 30})
    # plt.yscale('symlog')
    ax.tick_params(axis='both', which='major', labelsize=20)

    plt.show()

# %%
def plot_df_dphi(day):
    datess = []
    for i in range(len(day.time)):
        datess.append(datetime.utcfromtimestamp(day.time[i]))

    dates = matplotlib.dates.date2num(datess)

    fig,ax = plt.subplots(figsize=(27.2,9))

    ax.plot_date(dates, day.df_dphi, color='red', ls='-',lw=0.4,mew=0.4,ms=0.4)

    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%H:%M"))
    
    title = day.date + '    ' + r'$\dfrac{df}{dy}$'
    label_list = [r'$\dfrac{df}{dy}$']

    plt.title(title, fontsize=30)
    plt.legend(label_list)
    ax.tick_params(axis='both',which='major',labelsize=20)

    plt.show()

# %%
def plot_df_db(day):
    datess = []
    for i in range(len(day.time)):
        datess.append(datetime.utcfromtimestamp(day.time[i]))

    dates = matplotlib.dates.date2num(datess)

    fig,ax = plt.subplots(figsize=(27.2,9))

    ax.plot_date(dates, day.df_dB, color='red', ls='-',lw=0.4,mew=0.4,ms=0.4)

    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%H:%M"))
        
    tit = r'$\dfrac{df}{dB_x}$'

    plt.title(tit, fontsize=30)
    ax.tick_params(axis='both',which='major',labelsize=20)

    plt.show()

# %%
from math import log, log10

def rlog10(i):
    if (i > 0):
        return log10(i)
    else:
        return 0
        
def dt(i):
#     return np.pi/12*i
    return i

# %%
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
    
def find_internal_log(x):
    stamps = np.logspace(-2, 3, num=81)
    if (x < 10**(-2)):
        return 83
    
    for i in range(len(stamps)):
        if (stamps[i] > x):
            return i - 1
    
    return 83
    
def find_internal_norm(x):
    stamps = np.linspace(0,300,num=81)
        
    for i in range(len(stamps)):
        if (stamps[i] > x):
            return i - 1
        
    return 83

def fill_matrix_norm(day):
    ax = 'x'
    global total_matr_norm
    global matrix2
    
    matr_norm = np.zeros((80,80), dtype=np.int64)
    
    day.df_dT = [dt(elem) for elem in day.df_dphi]
    abs_df_dphi = [abs(elem) for elem in day.df_dphi]
    abs_df_dB = [abs(elem) for elem in day.df_dB]
    abs_f = [abs(elem) for elem in day.f]
    
    for i in range(len(day.df_dphi)):
        x = find_internal_norm(abs_df_dB[i])
        y = find_internal_norm(abs_f[i])
        if (x < 80 and y < 80):
            matr_norm[y][x] += 1
            total_matr_norm[y][x] += 1
            matrix2[y][x] += abs_df_dB[i]

    sums_norm = []
    
    for i in range(80):
        s = 0
        for k in range(0,10):
            s += matr_norm[k][i]
        sums_norm.append(s)
        
    x = np.arange(-2,3,1/16)

    plt.bar(x, height=sums_norm, align='edge', width = 1/16)
    x = np.arange(0,360,60) # the grid to which your data corresponds
    x_positions = np.arange(-2,4,1) # pixel count at label position
    x_labels = x[::1] # labels you want to see
    plt.xticks(x_positions, x_labels)
    name_image = '/home/fedor/Documents/HSE/Projects/Photos/HistNewY/Ynorm' + day.date + ax + '.png'
    plt.savefig(name_image)
    plt.show()
    
    matrix = np.array([[rlog10(elem) for elem in thing] for thing in matr_norm])

    nipy = cm.get_cmap('nipy_spectral', 512)
    newcmp = ListedColormap(nipy(np.linspace(0, 1, 512))).reversed()

    plt.imshow(matrix, cmap=newcmp, interpolation='none', origin='lower', extent=[-2,3,-2,3])
    plt.colorbar(orientation='vertical')
    
    x = np.linspace(0,300, num=81) # the grid to which your data corresponds
    x_positions = np.arange(-2,4,1) # pixel count at label position
    # x_labels = x[::1] # labels you want to see
    x_labels = []
    for i in range(0,81,16):
        x_labels.append(float(int(x[i]*100))/100)
        
    plt.xticks(x_positions, x_labels)        
    plt.yticks(x_positions, x_labels)
    
    plt.xlabel(r'$|\dfrac{df}{dB_x}|$', fontsize=18)
    plt.ylabel(r'$|f|$', fontsize=18)
    title = day.date
    plt.title(title, fontsize=30)
    print('Total occurrences:', len(day.f))
    
    name_image = '/home/fedor/Documents/HSE/Projects/Photos/ColorplotNewY/Ynorm' + day.date + ax + '.png'
    plt.savefig(name_image)
    plt.show()
    
    
def fill_matrix_log(day):
    ax = 'x'
    global total_matr_log
    
    matr_log = np.zeros((80,80), dtype=np.int64)
    
    day.df_dT = np.array([dt(elem) for elem in day.df_dphi])
    
    abs_df_dB = [abs(elem) for elem in day.df_dB]
    abs_df_dphi = [abs(elem) for elem in day.df_dphi]
    abs_f = [abs(elem) for elem in day.f]
    
    for i in range(len(day.df_dphi)):
        x = find_internal_log(abs_df_dB[i])
        y = find_internal_log(abs_f[i])
        if (x < 80 and y < 80):
            matr_log[y][x] += 1
            total_matr_log[y][x] += 1

    sums_log = []
    
    for i in range(80):
        s = 0
        for k in range(44,52):
            s += matr_log[k][i]
        sums_log.append(s)
        
    x = np.arange(-2,3,1/16)

    plt.bar(x, height=sums_log, align='edge', width = 1/16)
    x = np.arange(-2,4,1) # the grid to which your data corresponds
    x_positions = np.arange(-2,4,1) # pixel count at label position
    x_labels = x[::1] # labels you want to see
    plt.xticks(x_positions, x_labels)
    name_image = '/home/fedor/Documents/HSE/Projects/Photos/HistNewY/Ylog' + day.date + ax + '.png'
    plt.savefig(name_image)
    plt.show()
    
    matrix = np.array([[rlog10(elem) for elem in thing] for thing in matr_log])

    nipy = cm.get_cmap('nipy_spectral', 512)
    newcmp = ListedColormap(nipy(np.linspace(0, 1, 512))).reversed()

    stamps = np.logspace(-2, 3, num=81)

    plt.imshow(matrix, cmap=newcmp, interpolation='none', origin='lower', extent=[-2,3,-2,3])
    plt.colorbar(orientation='vertical')
    
    x = np.arange(-2,4,1) # the grid to which your data corresponds
    x_positions = np.arange(-2,4,1) # pixel count at label position
    x_labels = x[::1] # labels you want to see
    plt.yticks(x_positions, x_labels)
    
    y = np.arange(-2,4,1) # the grid to which your data corresponds
    y_positions = np.arange(-2,4,1) # pixel count at label position
    y_labels = y[::1] # labels you want to see
    plt.yticks(y_positions, y_labels)
    plt.xlabel(r'$|\dfrac{df}{dy}|$', fontsize=18)
    plt.ylabel(r'$|f|$', fontsize=18)
    title = day.date
    plt.title(title, fontsize=30)
    print('Total occurrences:', len(day.df_dT))
    
    name_image = '/home/fedor/Documents/HSE/Projects/Photos/ColorplotNewY/Ylog' + day.date + ax + '.png'
    plt.savefig(name_image)
    plt.show()

# %%
def build_full_matrix():
    ax = 'x'
    global total_matr_log
    global total_matr_norm
    
    sums_log = [] 
    sums_norm = []
    
    for i in range(80):
        s = 0
        for k in range(44,52):
            s += total_matr_log[k][i]
        sums_log.append(s)
        
    for i in range(80):
        s = 0
        for k in range(0,10):
            s += total_matr_norm[k][i]
        sums_norm.append(s)
        
#     x = list(np.logspace(-3, 1, num=17))
    x = np.arange(-2,3,1/16)

    plt.bar(x, height=sums_norm, align='edge', width = 1/16)
    x = np.arange(0,360,60) # the grid to which your data corresponds
    x_positions = np.arange(-2,4,1) # pixel count at label position
    x_labels = x[::1] # labels you want to see
    plt.xticks(x_positions, x_labels)
    name_image = '/home/fedor/Documents/HSE/Projects/Photos/HistNewY/YTotalNorm' + ax + '.png'
    plt.savefig(name_image)
    plt.show()
    
    
    total_matrix_norm = np.array([[rlog10(elem) for elem in thing] for thing in total_matr_norm])

    nipy = cm.get_cmap('nipy_spectral', 512)
    newcmp = ListedColormap(nipy(np.linspace(0, 1, 512))).reversed()

    plt.imshow(total_matrix_norm, cmap=newcmp, interpolation='none', origin='lower', extent=[-2,3,-2,3])
    plt.colorbar(orientation='vertical')
    
    x = np.linspace(0,300, num=81) # the grid to which your data corresponds
    x_positions = np.arange(-2,4,1) # pixel count at label position
    # x_labels = x[::1] # labels you want to see
    x_labels = []
    for i in range(0,81,16):
        x_labels.append(float(int(x[i]*100))/100)
    plt.xticks(x_positions, x_labels)        
    plt.yticks(x_positions, x_labels)

    plt.xlabel(r'$|\dfrac{df}{dB_x}|$', fontsize=18)
    plt.ylabel(r'$|f|$', fontsize=18)
#     title = day.date_of_day

    plt.title('Total', fontsize=30)
    
    total_occur_norm = 0
    
    for cols in total_matr_norm:
        for elem in cols:
            total_occur_norm += elem
    
#     print('Total occurances:', len(day.dfdb_ranged))
    print('Total occurrences norm:', total_occur_norm)
    
    name_image = '/home/fedor/Documents/HSE/Projects/Photos/ColorplotNewY/YTotalNorm' + ax + '.png'
    plt.savefig(name_image)
    plt.show()
    
    
    
    x = np.arange(-2,3,1/16)

    plt.bar(x, height=sums_log, align='edge', width = 1/16)
    x = np.arange(-2,4,1) # the grid to which your data corresponds
    x_positions = np.arange(-2,4,1) # pixel count at label position
    x_labels = x[::1] # labels you want to see
    plt.xticks(x_positions, x_labels)
    name_image = '/home/fedor/Documents/HSE/Projects/Photos/HistNewY/YTotalLog' + ax + '.png'
    plt.savefig(name_image)
    plt.show()
    
    
    total_matrix_log = np.array([[rlog10(elem) for elem in thing] for thing in total_matr_log])

    nipy = cm.get_cmap('nipy_spectral', 512)
    newcmp = ListedColormap(nipy(np.linspace(0, 1, 512))).reversed()

    stamps = np.logspace(-2, 3, num=81)

    plt.imshow(total_matrix_log, cmap=newcmp, interpolation='none', origin='lower', extent=[-2,3,-2,3])
    plt.colorbar(orientation='vertical')

    y = np.arange(-2,4,1) # the grid to which your data corresponds
    y_positions = np.arange(-2,4,1) # pixel count at label position
    y_labels = y[::1] # labels you want to see
    plt.yticks(y_positions, y_labels)
    plt.xlabel(r'$|\dfrac{df}{dB_x}|$', fontsize=18)
    plt.ylabel(r'$|f|$', fontsize=18)
#     title = day.date_of_day
    plt.title('Total', fontsize=30)
    
    total_occur_log = 0
    
    for cols in total_matr_log:
        for elem in cols:
            total_occur_log += elem
    
#     print('Total occurances:', len(day.dfdb_ranged))
    print('Total occurrences Log:', total_occur_log)
    
    name_image = '/home/fedor/Documents/HSE/Projects/Photos/ColorplotNewY/YTotalLog' + ax + '.png'
    plt.savefig(name_image)
    plt.show()

# %%
def append_to_day(day, s):
    try:
        time = date_to_float2(datetime.strptime(s[0], '%Y-%m-%d %H:%M:%S.%f'))
    except ValueError:
        time = date_to_float2(datetime.strptime(s[0], '%Y-%m-%d %H:%M:%S'))
    except:
        print('Something happened in day',day)

    day.time.append(time)

    AVx, AVy, AVz = float(s[1]), float(s[2]), float(s[3])
    Ax, Ay, Az = float(s[4]), float(s[5]), float(s[6])
    ABx, ABy, ABz = float(s[7]), float(s[8]), float(s[9])

    day.sat_a.V[0].append(AVx)
    day.sat_a.V[1].append(AVy)
    day.sat_a.V[2].append(AVz)

    day.sat_a.B[0].append(ABx)
    day.sat_a.B[1].append(ABy)
    day.sat_a.B[2].append(ABz)

    day.sat_a.pos[0].append(Ax)
    day.sat_a.pos[1].append(Ay)
    day.sat_a.pos[2].append(Az)

    DVx, DVy, DVz = float(s[10]), float(s[11]), float(s[12])
    Dx, Dy, Dz = float(s[13]), float(s[14]), float(s[15])
    DBx, DBy, DBz = float(s[16]), float(s[17]), float(s[18])

    day.sat_d.V[0].append(DVx)
    day.sat_d.V[1].append(DVy)
    day.sat_d.V[2].append(DVz)

    day.sat_d.B[0].append(DBx)
    day.sat_d.B[1].append(DBy)
    day.sat_d.B[2].append(DBz)

    day.sat_d.pos[0].append(Dx)
    day.sat_d.pos[1].append(Dy)
    day.sat_d.pos[2].append(Dz)


    EVx, EVy, EVz = float(s[19]), float(s[20]), float(s[21])
    Ex, Ey, Ez = float(s[22]), float(s[23]), float(s[24])
    EBx, EBy, EBz = float(s[25]), float(s[26]), float(s[17])

    day.sat_e.V[0].append(EVx)
    day.sat_e.V[1].append(EVy)
    day.sat_e.V[2].append(EVz)

    day.sat_e.B[0].append(EBx)
    day.sat_e.B[1].append(EBy)
    day.sat_e.B[2].append(EBz)

    day.sat_e.pos[0].append(Ex)
    day.sat_e.pos[1].append(Ey)
    day.sat_e.pos[2].append(Ez)
    
    day.denominator.append(float(s[33]))
    day.df_dphi.append(float(s[31]))
    day.df_dB.append(float(s[32]))
    day.Vx_ave.append(float(s[28]))
    day.Bx_ave.append(float(s[29]))
    day.phi_ave.append(float(s[30]))

# %%
import csv
from datetime import datetime

def perform_all():
    global Vx
    global Bx
    global df_dBx
    global Y
#     filename = 'SatelliteDataDate.csv'
    filename = 'SatelliteDataDateRes3.csv'
    
    with open(filename) as csvfile:
        
        readCSV = csv.reader(csvfile, delimiter=';')
        ind = 0
        for s in readCSV:
            if (ind == 0):
                wanted_day = datetime.strptime(s[0], '%Y-%m-%d %H:%M:%S.%f').date()
                date = s[0].split(' ')[0].replace('-','') # '2019-03-17 09:54:00.345345' to '20190317'
                day = Day(date)
                
            try:
                datetime_object = datetime.strptime(s[0], '%Y-%m-%d %H:%M:%S.%f')
            except ValueError:
                datetime_object = datetime.strptime(s[0], '%Y-%m-%d %H:%M:%S')

            curr_day = datetime_object.date()
            
            if (curr_day == wanted_day):
                append_to_day(day, s)
            
            if (ind > 0):
                try:
                    datetime_object = datetime.strptime(s[0], '%Y-%m-%d %H:%M:%S.%f')
                except ValueError:
                    datetime_object = datetime.strptime(s[0], '%Y-%m-%d %H:%M:%S')
                    
                curr_day = datetime_object.date()

                if (curr_day > wanted_day): # In case if new day
                    
                    fillindeces_and_all(day)
#                     delete_indeces(day)
                    print(day.date)
                    Vx.extend(day.Vx_ave)
#                     Vx.extend(day.sat_a.V[0])
                    Bx.extend(day.Bx_ave)
                    Bxd.extend(day.sat_d.B[0])
#                     Bx.extend(day.sat_a.B[0])
                    df_dBx.extend(day.df_dB)
                    Y.extend(day.phi_ave)
#                     Y.extend(day.sat_a.pos[1])
                    df_dY.extend(day.df_dphi)
#                     f.extend(day.f)
                    denominator.extend(day.denominator)
                    days.extend([day.date] * len(day.denominator))
#                     fill_matrix_norm(day)
#                     fill_matrix_log(day)
#                     build_v_b_ph_plots(day)

                    wanted_day = curr_day
                    date = s[0].split(' ')[0].replace('-','') # '2019-03-17 09:54:00.345345' to '20190317'
                    day = Day(date)

#                 print(datetime_object)

            ind += 1
            
#     build_full_matrix()

# %%
total_matr_norm = np.zeros((80,80), dtype=np.int64)
total_matr_log = np.zeros((80,80), dtype=np.int64)
matrix2 = np.zeros((80,80), dtype=np.int64)
Vx = []
Bx = []
Bxd = []
df_dBx = []
Y = []
df_dY = []
denominator = []
days = []
perform_all()
print('len of Vx is:', len(Vx))

# %%
build_full_matrix()

# %%
f = open("denominator_values.txt", "w")
for i in range(len(denominator)):
    s = str(days[i]) + ' ; ' + str(denominator[i]) + ' ; ' + str(Bx[i]) + '\n'
    f.write(s)
    
f.close()

# %%
def delete_indeces_V_B(Vx, Bx, df_dBx, df_dY, indeces):
    k = len(indeces)
    for i in range(k - 1, -1, -1):
        del Vx[indeces[i]]
        del Bx[indeces[i]]
        del df_dBx[indeces[i]]
        del Y[indeces[i]]
        del df_dY[indeces[i]]
        print(i)

# %%
def find_indeces(Vx, Bx, df_dBx):
    indeces = []
    for i in range(len(Vx)):
        if ((Vx[i] < 50 and Vx[i] > -50)):
            indeces.append(i)
            print('I delete because ', Vx[i])
        
    return indeces

# %%
# indeces = find_indeces(Vx, Bx, df_dBx)
# print('Len of indeces', len(indeces))
# print('Before delition')
# print('Len of Vx', len(Vx), 'Len of Bx', len(Bx), 'Len of derivatives Bx, Y', len(df_dBx), len(df_dY), 'Len Y', len(Y))
# delete_indeces_V_B(Vx, Bx, df_dBx, df_dY, indeces)
# print('After delition')
# print('Len of Vx', len(Vx), 'Len of Bx', len(Bx), 'Len of derivatives Bx, Y', len(df_dBx), len(df_dY), 'Len Y', len(Y))

# %%
Bx = [abs(i) for i in Bx]

# %%
values_Vx = np.linspace(-50,50,80)
values_Bx = np.linspace(-75,75,80)

with open("Vx_values.txt", "w") as outfile:
    outfile.write("\n".join(str(item) for item in Vx))
    
with open("Bx_values.txt", "w") as outfile1:
    outfile1.write("\n".join(str(item) for item in Bx))
    
f = open("df_dBx_values.txt", "w")
for i in range(len(df_dBx)):
    s = str(days[i]) + ';' + str(df_dBx[i]) + '\n'
    f.write(s)
    
f.close()

with open("Y_values.txt", "w") as outfile2:
    outfile2.write("\n".join(str(item) for item in Y))
    
f = open("df_dY_values.txt", "w")
for i in range(len(df_dY)):
    s = str(days[i]) + ';' + str(df_dY[i]) + '\n'
    f.write(s)
    
f.close()

f = open("derivatives.txt", "w")
for i in range(len(df_dY)):
    s = str(days[i]) + ';' + str(df_dY[i]) + ';' + str(df_dBx[i]) + ';' + str(Vx[i]) + ';' + str(Bx[i]) + ';' + str(Y[i]) + ';' + str(denominator[i]) + '\n'
    f.write(s)
    
f.close()


# %%
print(min(Y))

# %%
def find_index(elem, array):
    
    for i in range(len(array)):
        if (array[i] > elem):
            return i - 1
        
    print(elem)
    
    return 2

# %%
matrix_1 = np.zeros([4,4], dtype=np.int64)
matrix_a = np.zeros([4,4], dtype=np.float64)

arr_vx = [min(Vx), -150, -50, 50, 150, max(Vx)]
arr_bx = [0, 10, 20, 30, max(Bx)]
arr_y = [min(Y), -31900, 0, 31900, max(Y)]

for i in range(len(Bx)):
    bx = Bx[i]
    vx = Vx[i]
    y = Y[i]
    df_dbx = df_dBx[i]
    df_dy = df_dY[i]
    
#     ind_bx = find_index(bx, arr_bx)
    ind_y = find_index(y, arr_y)
    ind_vx = find_index(vx, arr_vx)

    if (ind_vx != 2):
    
        if (ind_vx > 2):
            ind_vx -= 1
    
        matrix_1[ind_vx][ind_y] += 1
        matrix_a[ind_vx][ind_y] += df_dy
#         matrix_1[ind_vx][ind_bx] += 1
#         matrix_a[ind_vx][ind_bx] += df_dbx
        
matrix_2 = np.divide(matrix_a, matrix_1)

# %%
print(matrix_1)
print('\n'.join('; '.join(str(col) for col in row) for row in matrix_1))
print('#######################################################')
print(matrix_a)
print('########################################################')
print(matrix_2)
print('\n'.join('; '.join(str(col) for col in row) for row in matrix_2))

# %%
matrix_1[1][1]
print(max(df_dY))

# %%
matrix_df_db = []
for i in range(4):
    matrix_df_db.append([]) # appending rows

for i in range(4):
    for s in range(4):
        matrix_df_db[i].append([]) # for each row append column
        
for i in range(len(Bx)):
    bx = Bx[i]
    vx = Vx[i]
    df_dbx = df_dBx[i]
    df_dy = df_dY[i]
    y = Y[i]
    
#     ind_bx = find_index(bx, arr_bx)
    ind_y = find_index(y, arr_y)
    ind_vx = find_index(vx, arr_vx)
    
    if (ind_vx != 2):
    
        if (ind_vx > 2):
            ind_vx -= 1
            
#         matrix_df_db[ind_vx][ind_bx].append(df_dbx)
        matrix_df_db[ind_vx][ind_y].append(df_dy)

# %%
import math
def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper

def do_for_cell(cell):
    new_ar_df_db = cell
    
#     print(new_ar_df_db)

    array = np.linspace(-150, 150, num=50)

    indeces_df_db = []
    
    sum_values = 0
    len_values = 0
    
    for i in range(len(new_ar_df_db)):
        elem = new_ar_df_db[i]
        if (elem < array[-1] and elem > array[0]):
            for k in range(len(array)):
                if (array[k] > elem):
                    sum_values += elem
                    len_values += 1
                    indeces_df_db.append(k - 1)
                    break

    # indeces_df_db = [find_index(elem, mesh_df_db) for elem in new_ar_df_db]

    mid = sum(indeces_df_db)/len(indeces_df_db)
    
    for k in range(len(array)):
        if (array[k] > mid):
            mid_index = k
            break

    density_df_db = np.zeros(50)

    for elem in indeces_df_db:
        density_df_db[elem] += 1
        
        
    return density_df_db, mid

# %%
def plot_dens(density_df_db, num):
    leni = np.linspace(-150, 150, num=50)
    plt.bar(leni, density_df_db, width = 300/50)
    s = r'$\dfrac{df}{d|Y|}$' + '      occurrences: ' + str(int(sum(density_df_db)))
    plt.title(s)
    plt.show()

# %%
def find_range(density_df_db, mid):
    mid = 0
    s = 0
    mid_val = 0
    for i in range(len(density_df_db)):
        if (density_df_db[i] > mid_val):
            mid_val = density_df_db[i]
            mid = i
        s += density_df_db[i]
        
    mean = s / 50 - 150
    
    i = 1
    delta = 0
    while (i == 1 and mid + delta > 0 and mid + delta < 50):
        delta += 1
        a = density_df_db[mid]/density_df_db[mid + delta]
        if (a >= np.e):
            i = 2

    pos_right_e = mid + delta

    i = 1
    delta = 0
    while (i == 1 and mid + delta > 0 and mid + delta < 50):
        delta -= 1
        if (density_df_db[mid]/density_df_db[mid + delta] >= np.e):
            i = 2

    pos_left_e = mid + delta

    a = int(mid*6)
    b = int(pos_left_e*6)
    c = int(pos_right_e*6)

    print('mid in', a - 150, end = ' ')
    print('left e is', b - 150, end = ' ')
    print('right e is', c - 150, end = ' ')

# %%
def do_all(cell, num):
    density_df_db, mid = do_for_cell(cell)
    plot_dens(density_df_db, num)
    find_range(density_df_db, mid)

# %%
for v in range(4):
    for y in range(4):
        print()
        print('cell[v==', v, '] [b==', y,']', sep='')
        num = str(matrix_1[v][y])
        do_all(matrix_df_db[v][y], num)

# %%
matrix_3a = np.zeros([4,4], dtype=np.float64)

for rows in range(4):
    for cols in range(4):
        s = 0
        for i in range(len(matrix_df_db[rows][cols])):
            s += (matrix_df_db[rows][cols][i] - matrix_2[rows][cols])**2
        matrix_3a[rows][cols] = s
        
matrix_3 = np.divide(matrix_3a, matrix_1)

# %%
import math
def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper

print(matrix_3)
print('\n'.join('; '.join(str(col) for col in row) for row in matrix_3))
matrix_3_truncated = np.array([[truncate(elem, 6) for elem in row] for row in matrix_3])
print('##############################')
print(matrix_3_truncated)

# %%
matr_st_dev = np.power(matrix_3, 0.5)

# %%
print(matr_st_dev)
print('\n'.join('; '.join(str(col) for col in row) for row in matr_st_dev))
print('################################')
matr_st_dev_truncated = np.array([[truncate(elem, 6) for elem in row] for row in matr_st_dev])
print(matr_st_dev_truncated)


# %%
ef find_nearest(val, array):
    for i in range(len(array)):
        if (array[i] > val):
            index = i
            break
    
    if (array[index] - val > val - array[index-1]):
        return index - 1
    else:
        return index

# %%
new_total_matr_norm = np.zeros((80,80), dtype=np.float64)

occurrences = []
for row in range(0,80,16):
    sum = 0
    
    for i in range(16):
        for elem in range(0,80):
            sum += total_matr_norm[row + i][elem]
            
    occurrences.append(sum)
    
    for i in range(16):
        for elem in range(0,80):
            new_total_matr_norm[row + i][elem] = total_matr_norm[row + i][elem]/sum
        
ind = 0
for row in range(0,80,16):
    sums = []
    
    x_initial = np.linspace(0, 300, num=80)
    x_sqrt = np.array([i**(1/2) for i in x_initial])
    
    for i in range(0,80):
        s = 0
        value = i * delta
        index_sq = find_nearest(value, x_squares)
        
        for m in range(0,16):
            s += new_total_matr_norm[m + row][i]
        sums.append(s)
        
    sumslog = np.array([rlog10(elem) for elem in sums])

        #     x = list(np.logspace(-3, 1, num=17))
    
    plt.bar(x_sqrt, height=sumslog, align='edge', width = 80/300)
    
    title = 'rows ' + str(row) + '-' + str(row+15) + ' occurrences: ' + str(occurrences[ind])
    plt.title(title, fontsize=25)

#         name_image = '/home/fedor/Documents/HSE/Projects/Photos/HistNewY/YTotalNorm' + ax + '.png'
#         plt.savefig(name_image)
    plt.show()
    ind += 1        

# %%
def Matrix1(total_matr_norm):
    return total_matr_norm

def collect_matrices():
    # На вход даётся то, по чему мы потом строим colormap в нормальном масштабе.
    global total_matr_norm
    global matrix2
    # Matrix1 -> contains number of events in each cell. It is basically already calculated matrix
    matrix1 = Matrix1(total_matr_norm)
    # Matrix2 -> contains summed values of df/dBx for that cell
    # Than means can be calculated as Matrix2[i][j]/Matrix1[i][j] in each cell
    means = np.divide(matrix2, matrix1)
    
    # Matrix3 -> contains (df/dBx - mean)^2
    

# %%


# %%
