First should be ran program Derivatives_tocsv_intervals.py  - it finds
appropriate intervals for one year and writes into file

That file should be ran for all years 2011-2019. As a result there will be
9 different files, which should be conacatenated into single big file.

For that big file, we should calculate average values of Vx, Bx, Y and values
of derivatives and write then to new file.

Program delete_points deletes all points which should not be considered further.

After all programs ran, we ran program FromCSVExpVar, whcich finds the matrix
for the variances and plots histograms corresponding to values of derivatives.
