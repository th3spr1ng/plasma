import csv
from datetime import datetime

def get_range(B_i, phi_i, B_s, phi_s):
    return ((B_i-B_s)**2 + (phi_i-phi_s)**2)**(1/2)

def perform_all():
    days = set()

    filenamein = 'SatelliteDataDateR.csv'
    filenameout = 'SatelliteDataDateRes3.csv'
    outfile = open(filenameout, "w")
    i = 0

    day_prev = '20110101'

    with open(filenamein) as csvfile1:
        gap = 10
        jump = False
        readCSV = csv.reader(csvfile1, delimiter=';')
        ind = 0
        for s in readCSV:
            to_write = True
            Vx_ave = float(s[28])
            Bx_ave = float(s[29])
            Y_ave = float(s[30])
            df_dphi = float(s[31])
            df_db = float(s[32])
            denominator = float(s[33])
            day = s[0].split(' ')[0].replace('-','')

            if (day > day_prev):
                day_prev = day
                gap = 5

            f = [s[1], s[10], s[19]]
            Bx = [s[7], s[16], s[25]]
            Y = [s[5], s[14], s[23]]
            
            f = [float(i) for i in f]
            Bx = [float(i) for i in Bx]
            Y = [float(i) for i in Y]

            f_ave = sum(f)/3
            fk = [i/f_ave for i in f]
            Bk = [i/Bx_ave for i in Bx]
            Yk = [i/Y_ave for i in Y]
            
            ranges1 = []
            for k in range(0,3):
                ranges1.append(get_range(Bk[k], Yk[k], Bk[k-1], Yk[k-1]))

            ranges2 = []
            for k in range(0,3):
                ranges2.append(get_range(Bk[k], fk[k], Bk[k-1], fk[k-1]))

            ranges3 = []
            for k in range(0,3):
                ranges3.append(get_range(fk[k], Yk[k], fk[k-1], Yk[k-1]))

            if (max(ranges1) / min(ranges1) > 5 or abs(denominator) <= 0.001):
                to_write = False

            if (max(ranges2) / min(ranges2) > 5):
                to_write = False

            if (max(ranges3) / min(ranges3) > 5):
                to_write = False

            # if (abs(Vx_ave) < 50):
            #     to_write = False

            if ('2018-03-29' in day):
                to_write = False

            if (gap > 0 and (abs(denominator) >= 10000)):
                gap = 10
                to_write = False

            if (abs(denominator) < 10000):
                gap -= 1

            if (abs(Bx_ave) > 1000):
                to_write = False
                days.add(day)
                i += 1

            if (to_write):
                m = (';').join([str(item) for item in s])
                m = m + '\n'
                outfile.write(m)

    print(len(days))
    print('days are ###############')
    print(*days)
    print('###########')
    print(i)
    outfile.close()
    # print(len(line))

perform_all()